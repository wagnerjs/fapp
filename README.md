# FApp

An fake app to simulate impact of database queries in an Application.

Based on Flask and Swagger.

# Requirements

- python 3.4
- python-setuptools

On Linux OS run:

```
sudo apt-get install libmysqlclient-dev
```

On MacOS run:

```
brew install mysql-connector-c
```

# Installation

Clone this repo and inside project folder with Inside your virtualenv (or not) run:

```
pip install -e .
```

# Usage

After installation start run `fakeapp` on terminal and access http://your\_machine\_ip:5000 to api or http://your\_machine\_ip:5000/apidocs to SwaggerUI.

# Supported databases

- MySQL

