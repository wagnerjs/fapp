from distutils.cmd import Command
from setuptools import find_packages, setup
import os
import stat


class DaemonInstall(Command):
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        file_data = """
        #!/bin/sh

        DAEMON=fakeapp
        DAEMON_NAME=fakeapp
        DAEMON_OPTS=""

        # Root is needed to use the GPIO pins on the Raspberry
        DAEMON_USER=root

        PIDFILE=/var/run/$DAEMON_NAME.pid

        do_start () {
            echo "Starting system $DAEMON_NAME daemon"
            start-stop-daemon --start --background --pidfile $PIDFILE --make-pidfile --user $DAEMON_USER --chuid $DAEMON_USER --startas $DAEMON -- $DAEMON_OPTS
        }
        do_stop () {
            echo "Stopping system $DAEMON_NAME daemon"
            start-stop-daemon --stop --pidfile $PIDFILE --retry 10
        }

        case "$1" in

            start|stop)
                do_${1}
                ;;

            restart|reload|force-reload)
                do_stop
                do_start
                ;;

            status)
                status_of_proc "$DAEMON_NAME" "$DAEMON" && exit 0 || exit $?
                ;;

            *)
                echo "Usage: service $DAEMON_NAME {start|stop|restart|status}"
                exit 1
                ;;

        esac
        exit 0\n"""

        daemon_file = '/etc/init.d/fakeapp'
        with open(daemon_file, 'w') as f:
            f.write(file_data)

        st = os.stat(daemon_file)
        os.chmod(daemon_file, st.st_mode | stat.S_IEXEC)

setup(
    name='fapp',
    version='0.1',
    install_requires=[
        'flasgger',
        'flask-mysqldb',
    ],
    packages=find_packages(),
    author='Wagner Santos',
    author_email='wagner.santos@zerum.com',
    entry_points={
        'console_scripts': [
            'fakeapp=fapp.app:main'
        ]
    },
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 3.4',
    ],
    keywords='api flask ',
    cmdclass={
        'install_daemon': DaemonInstall
    }
)

