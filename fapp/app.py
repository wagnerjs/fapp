from flask import Flask, jsonify, request
from flask_mysqldb import MySQL
from flasgger import Swagger

app = Flask(__name__)
swagger = Swagger(app)
mysql = MySQL(app)

################################# Globals #####################################

DB_HOST = '192.168.203.214'
DB_PORT = 3306
DB_NAME = 'hackazon'
DB_USER = 'root'
DB_PASS = 'default'

################################## Rotes ######################################

app.config.update(
    MYSQL_HOST=     DB_HOST,
    MYSQL_USER=     DB_USER,
    MYSQL_PASSWORD= DB_PASS,
    MYSQL_DB=       DB_NAME,
)

@app.route('/table', methods=['GET'])
def list_tables():
    """Endpoint return a list of tables in database
    ---
    definitions:
        Table:
            type: string
            properties:
                tablename:
                    type: string
    """

    sql_stmt = 'SHOW TABLES'
    cur = mysql.connection.cursor()
    cur.execute(sql_stmt)
    rv = cur.fetchall()

    return jsonify(rv)

@app.route('/query', methods=['POST'])
def query():
    """Endpoint to describe table
    ---
    parameters:
      - name: query
        in: body
        required: true
        description: Query or queries to execute
        schema:
          required:
            - query
          properties:
            query:
              default: [SELECT * FROM tablename]
    """

    query = request.get_json()['query']

    if type(query) is str:
        query = [query]


    result = []
    for q in query:
        cur = mysql.connection.cursor()
        cur.execute(q)
        rv = cur.fetchall()
        result.append(rv)

    return jsonify(result)


def main():
    app.run(
        host='0.0.0.0',
        port=5000,
        debug=True
    )


if __name__ == '__main__':
    main()
